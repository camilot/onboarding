﻿using Console_Application__Onboarding_.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Application__Onboarding_.Data_Access
{
    class OrderManager
    {
        OnboardingTestEntities context;

        public OrderManager()
        {
            context = new OnboardingTestEntities();
        }

        public int CheckOrderID(string orderName)
        {
            try
            {
                int orderId = -1;
                Order order = context.Orders.Where(x => x.Name == orderName).FirstOrDefault();
                if(order != null)
                {
                    orderId = order.ID;
                }

                return orderId;
            }
            catch(Exception e)
            {
                return -1;
            }
        }
    }
}

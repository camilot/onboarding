﻿using Console_Application__Onboarding_.Data_Access;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Application__Onboarding_.Administrator
{
    class OrderBusinessLogic
    {
        OrderManager orderManager;
        
        public OrderBusinessLogic()
        {
            orderManager = new OrderManager();
        }

        public int CheckOrderName(string orderName)
        {
            try
            {
                int orderId = this.orderManager.CheckOrderID(orderName);
                return orderId;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
    }
}

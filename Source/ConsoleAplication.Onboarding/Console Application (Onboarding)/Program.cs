﻿using Console_Application__Onboarding_.Administrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Application__Onboarding_
{
    class Program
    {
        /*Method designed to clear charging message*/
        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        static void Main(string[] args)
        {
            OrderBusinessLogic orderAdministrator = new OrderBusinessLogic();

            Console.WriteLine("Enter order name: ");
            string orderName = Console.ReadLine();

            Console.WriteLine("Consulting....");

            int orderId = orderAdministrator.CheckOrderName(orderName);

            Console.SetCursorPosition(0, Console.CursorTop - 1);
            ClearCurrentConsoleLine();

            Console.WriteLine("\nResult: Order ID {0}", orderId);

            Console.ReadLine();
        }
    }
}
